package com.boilerplate.tasks.task1;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Wojciech Koszycki
 */
public class CountFarthestAbsNumberPositionFromAverageTest {

    CountFarthestAbsNumberPositionFromAverage instance;

    @Before
    public void setUp() {
        instance = new CountFarthestAbsNumberPositionFromAverage();
    }

    @Test
    public void testSmallArray() {
        int[] input = new int[]{1, 2, 3, 5};
        assertEquals(3, instance.count(input));
    }

}
