package com.boilerplate.tasks.task3;

import java.util.Arrays;
import static junit.framework.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Wojciech Koszycki
 */
public class ComplimentaryTest {

    Complimentary complimentary;

    @Before
    public void setUp() {
        complimentary = new Complimentary();
    }

    @Test
    public void testSmallArray() {
        int[] input = new int[]{1, 8, -3, 0, 1, 3, -2, 4, 5};
        int result = complimentary.countComplimentaryPairs(6, input);
        assertEquals(7, result);
    }

    @Test
    public void testOneElement() {
        int[] input = new int[]{1};
        int result = complimentary.countComplimentaryPairs(1, input);
        assertEquals(0, result);
    }

    @Test
    public void testTwoElements() {
        int[] input = new int[]{1, 2};
        int result = complimentary.countComplimentaryPairs(1, input);
        assertEquals(0, result);
        int result2 = complimentary.countComplimentaryPairs(3, input);
        assertEquals(2, result2);
    }

    @Test
    public void testLargeValues() {
        int n = 40000;
        int[] input = new int[n];
        Arrays.fill(input, 0, n, 5);
        int result = complimentary.countComplimentaryPairs(10, input);
        assertEquals(n * n, result);
    }
}
