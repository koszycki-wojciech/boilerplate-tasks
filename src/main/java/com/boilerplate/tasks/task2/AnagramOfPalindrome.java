package com.boilerplate.tasks.task2;

/**
 * anagram = RaceFastSafeCar , palindrome = just messed up sequence of chars
 * Firstable we need to analyse anagram e.g abccba or abcdcba = there can be
 * only one odd letter inside otherwise IT IS NOT anagram e.g
 *
 * @author Wojciech Koszycki
 */
public class AnagramOfPalindrome {

    public boolean isAnagramPalindrome(String anagram) {
        //Need it to treat all chars the same
        anagram = anagram.toLowerCase();
        int lastIndexOfAlphaebt = Character.getNumericValue('z');
        // prepare array to count each char occurence
        int[] charOccurences = new int[lastIndexOfAlphaebt];
        // count each letter
        //only one can be even
        int countOddNumbers = 0;
        for (char singleChar : anagram.toCharArray()) {
            int index = Character.getNumericValue(singleChar);
            charOccurences[index] = charOccurences[index] + 1;
            System.out.println(charOccurences[index]);
            if ((charOccurences[index] & 1) != 0) {
                ++countOddNumbers;
            } else {
                --countOddNumbers;
            }

        }
        return countOddNumbers < 2;
    }

}
