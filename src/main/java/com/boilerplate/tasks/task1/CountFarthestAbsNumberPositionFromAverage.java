package com.boilerplate.tasks.task1;

/**
 * Task is to find position (index) of number that has biggest absolute
 * difference from average e.g 1,2,3,5=11 avg=2,75 farhestElement=5 indexOf5=3
 *
 * @author Wojciech Koszycki
 */
public class CountFarthestAbsNumberPositionFromAverage {

    public int count(int[] input) {

        int min = input[0];
        int indexMin = 0;
        int max = input[0];
        int indexMax = 0;
        long sumTotal = input[0];
        for (int i = 1; i < input.length; i++) {
            int currentElement = input[i];
            sumTotal += Math.abs(currentElement);
            // find minimum element
            if (currentElement < min) {
                min = currentElement;
                indexMin = i;
            }
            //find maximum element
            if (currentElement > max) {
                max = currentElement;
                indexMax = i;
            }
        }
        // count average
        double avg = (double) sumTotal / input.length;
        //check which one has bigger difference
        int index = Math.abs(avg - min) > Math.abs(avg - max) ? indexMin : indexMax;
        return index;
    }

}
