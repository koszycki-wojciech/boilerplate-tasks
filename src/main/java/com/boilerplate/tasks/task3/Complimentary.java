package com.boilerplate.tasks.task3;

import java.util.HashMap;
import java.util.Map;

/**
 * Task is to count pairs in array a,b that a+b=complimentary e.g. input={2,5,8}
 * and complimentary=10 contains pairs:(2,8),(5,5),(8,2), so the function should
 * return three,
 *
 * @author Wojciech Koszycki
 */
public class Complimentary {

    int countComplimentaryPairs(int complimentary, int[] input) {
        final int size = input.length;
        if (size < 2) {
            return 0;
        }
        Map<Integer, Integer> valuesThatWeNeed = new HashMap<>(size);
        //Count occurences of each element
        for (int i = 0; i < size; i++) {
            //we add value which, we are looking for
            //eg. complimentary=6,input[i]=1 , so then we will look for 5 right, if it occur 3 times ?
            final int neededValue = (complimentary) - input[i];
            int howManyWeNeed = valuesThatWeNeed.containsKey(neededValue) ? valuesThatWeNeed.get(neededValue) : 0;
            valuesThatWeNeed.put(neededValue, howManyWeNeed + 1);
        }

        int numberOfPairs = 0;
        for (int i = 0; i < size; i++) {
            final int currentValue = input[i];
            //check if current value is one of the values that we need
            numberOfPairs += valuesThatWeNeed.containsKey(currentValue) ? valuesThatWeNeed.get(currentValue) : 0;
        }

        return numberOfPairs;
    }

}
